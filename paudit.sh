#! /usr/bin/env bash

set -e # Causes script to terminate upon command failure

# Test for $EDITOR
if [ "$EDITOR" == "" ]; then
	printf "Please set the EDITOR environment variable.\n"
	exit 1
fi

# Make temp folder
DATE=`date '+%H-%M-%S'`
TMPATH="/tmp/paudit-$DATE"
mkdir "$TMPATH"

# Get installed packages
comm -23 <(pacman -Qeq | sort) <(pacman -Qgq base base-devel | sort) > "$TMPATH/packages.txt"
cp "$TMPATH/packages.txt" "$TMPATH/wanted.txt"

# Warning
printf "Disclaimer: \033[1mif you delete something important, it's not my fault.\033[0m\n"
printf "Delete the packages from the list in order to mark them for removal.\nGot it? (Y or N). "
read USER_BEGIN

if [ "$USER_BEGIN" != y ] && [ "$USER_BEGIN" != "Y" ]; then
	exit 1
fi

# Let user delete packages
$EDITOR "$TMPATH/wanted.txt"

if [ "$(diff -q $TMPATH/packages.txt $TMPATH/wanted.txt)" == "" ]; then
	printf "No changes to list.\n"
else

	comm -23 <(sort $TMPATH/packages.txt) <(sort $TMPATH/wanted.txt) > "$TMPATH/to_delete.txt" # Like diff, but without the other crap it sticks in

	cat "$TMPATH/to_delete.txt"
	printf "Really delete these packages? Y or N. "
	read USER_CONT

	if [ "$USER_CONT" == "y" ] || [ "$USER_CONT" == "Y" ]
	then
		sudo pacman -Rsn - < "$TMPATH/to_delete.txt"
	fi
fi
